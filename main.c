/*
 * main.c
 *
 *  Created on: Feb 18, 2020
 *      Author: burak.kirazli
 */

#include "unity_fixture.h"

TEST_GROUP_RUNNER(circular)
{
	//baundary tests
	RUN_TEST_CASE(circular, test_ctor);
	RUN_TEST_CASE(circular, test_single_byte_insert);
	RUN_TEST_CASE(circular, test_single_byte_receive);
	RUN_TEST_CASE(circular, test_single_block_insert);
	RUN_TEST_CASE(circular, test_make_full);
	RUN_TEST_CASE(circular, test_receive_block_amount_equals_inserted);
	RUN_TEST_CASE(circular, test_receive_more_than_available);
	RUN_TEST_CASE(circular, test_receive_less_than_available);
	RUN_TEST_CASE(circular, test_single_byte_stream);
	RUN_TEST_CASE(circular, test_block_stream);
	RUN_TEST_CASE(circular, test_block_stream_receive_always_more_than_available);

	//data integrity tests
	RUN_TEST_CASE(circular, test_byte_stream_integrity);
	RUN_TEST_CASE(circular, test_block_stream_integrity);

}

void runner(void)
{
	RUN_TEST_GROUP(circular);
}

int main(int argc, const char* argv[])
{
	return UnityMain(argc, argv, runner);
}
