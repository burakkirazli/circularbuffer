/*
 * circular.c
 *
 *  Created on: Feb 18, 2020
 *      Author: burak.kirazli
 */

#include "circular.h"
#include <string.h>

struct _circular_t
{
	char* buffer;
	int buffer_size;
	volatile int head;
	volatile int tail;
	volatile int is_full;
	circular_buffer_oveflow_detected_cb_t cb;
};

//
static struct _circular_t container[CIRCULAR_CONTAINER_SIZE];
static short container_idx = 0;

//
static circular_buffer_oveflow_detected_cb_t ovdcb = 0;

//
circular_t circular_ctor(char* buffer, int size)
{
	if(container_idx < CIRCULAR_CONTAINER_SIZE)
	{
		container_idx++;
		circular_t instance = &container[container_idx - 1];
		instance->buffer_size = size;
		instance->buffer = buffer;
		instance->head = 0;
		instance->tail = 0;
		return instance;
	}

	return 0;
}

void circular_reset(circular_t circ)
{
	for(int i = 0; i < CIRCULAR_CONTAINER_SIZE; i++)
	{
		if(circ == &container[i])
		{
			circ->head = 0;
			circ->tail = 0;
			circ->is_full = 0;
			break;
		}
	}
}

void circular_set_overflow_detected_cb(circular_buffer_oveflow_detected_cb_t cb)
{
	ovdcb = cb;
}

void circular_reset_container(void)
{
	container_idx = 0;
	memset(container, 0, sizeof(container));
}

int circular_get_size(circular_t circ)
{
	return circ->buffer_size;
}

char* circular_get_buffer(circular_t circ)
{
	return circ->buffer;
}

static inline void move_head(circular_t circ)
{
	circ->head = (circ->head + 1) % circ->buffer_size;
	if(circ->head == circ->tail)
	{
		circ->is_full = 1;
		if(ovdcb)
		{
			ovdcb(circ);
		}
	}
}

static inline void move_tail(circular_t circ)
{
	circ->tail = (circ->tail + 1) % circ->buffer_size;
}

static inline int insert_byte(circular_t circ, char * byte)
{
	//
	if(circular_space_available(circ) < 1)
	{
		return 0;
	}

	//
	*(circ->buffer + circ->head) = *byte;

	//
	move_head(circ);

	//
	return 1;
}

static inline int receive_byte(circular_t circ, char* buffer)
{
	//
	if(circular_bytes_available(circ) <= 0)
	{
		return 0;
	}

	//
	*buffer = *(circ->buffer + circ->tail);

	//
	move_tail(circ);

	//
	return 1;
}


int circular_insert(circular_t circ, char* data, int size)
{
	int index = 0;

	while(index < size)
	{
		if(insert_byte(circ, data + index) != 1)
		{
			break;
		}

		index++;
	}

	return index;
}

int circular_bytes_available(circular_t circ)
{
	if(circ->is_full)
	{
		return circ->buffer_size;
	}

	if(circ->head - circ->tail >= 0)
	{
		return circ->head - circ->tail;
	}

	return (circ->buffer_size - (circ->tail - circ->head));
}

int circular_space_available(circular_t circ)
{
	return (circ->buffer_size - circular_bytes_available(circ));
}

int circular_receive(circular_t circ, char* buffer, int size)
{
	int amount = 0;

	while(receive_byte(circ, buffer + amount) > 0)
	{
		amount++;

		if(amount == size)
		{
			break;
		}
	}

	return amount;
}
