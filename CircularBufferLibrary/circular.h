/*
 * circular.h
 *
 *  Created on: Feb 18, 2020
 *      Author: burak.kirazli
 */

#ifndef CIRCULARBUFFERLIBRARY_CIRCULAR_H_
#define CIRCULARBUFFERLIBRARY_CIRCULAR_H_

//
#define CIRCULAR_CONTAINER_SIZE 10

//
typedef struct _circular_t* circular_t;
typedef void (*circular_buffer_oveflow_detected_cb_t)(circular_t circ);

//constructor related
circular_t circular_ctor(char* buffer, int buffer_size);
void circular_reset(circular_t circ);
void circular_set_overflow_detected_cb(circular_buffer_oveflow_detected_cb_t cb);
void circular_reset_container(void);

//instance related
int circular_get_size(circular_t circ);
char* circular_get_buffer(circular_t circ);

//inserters
int circular_insert(circular_t circ, char* data, int size);

//available ites
int circular_bytes_available(circular_t circ);
int circular_space_available(circular_t circ);

//receive
int circular_receive(circular_t circ, char* buffer, int size);



#endif /* CIRCULARBUFFERLIBRARY_CIRCULAR_H_ */
