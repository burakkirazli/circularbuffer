/*
 * circular_test.c
 *
 *  Created on: Feb 18, 2020
 *      Author: burak.kirazli
 */

#include "unity_fixture.h"
#include "../CircularBufferLibrary/circular.h"
#include "string.h"

//
static char _100BytesBuffer[100];
static int _100BytesBufferSize = 100;
static int overflow_detection_flag = 0;
void overflow_detection_cb(circular_t circ)
{
	overflow_detection_flag = 1;
}

static circular_t _100BytesCtor(void)
{
	circular_t circ = circular_ctor(_100BytesBuffer, _100BytesBufferSize);
	circular_set_overflow_detected_cb(overflow_detection_cb);
	return circ;
}

TEST_GROUP(circular);

TEST_SETUP(circular)
{
	overflow_detection_flag = 0;
}

TEST_TEAR_DOWN(circular)
{
	circular_reset_container();
}



TEST(circular, test_ctor)
{
	//
	circular_t circ = _100BytesCtor();
	TEST_ASSERT_NOT_EQUAL(0, circ);

	//size test
	TEST_ASSERT_EQUAL(_100BytesBufferSize, circular_get_size(circ));

	//buffer test
	TEST_ASSERT_EQUAL(_100BytesBuffer, circular_get_buffer(circ));
}

TEST(circular, test_single_byte_insert)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char byte_data = 'k';

	//
	TEST_ASSERT_EQUAL(1, circular_insert(circ, &byte_data, 1));
	TEST_ASSERT_EQUAL(_100BytesBufferSize - 1, circular_space_available(circ));
	TEST_ASSERT_EQUAL(1, circular_bytes_available(circ));
}

TEST(circular, test_single_byte_receive)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char test_char = 'k';
	circular_insert(circ, &test_char, 1);

	//
	char buffer;
	TEST_ASSERT_EQUAL(1, circular_receive(circ, &buffer, 1));
	TEST_ASSERT_EQUAL(0, circular_bytes_available(circ));
	TEST_ASSERT_EQUAL(_100BytesBufferSize, circular_space_available(circ));
}

TEST(circular, test_single_block_insert)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data[50];

	//
	TEST_ASSERT_EQUAL(50, circular_insert(circ, data, 50));
	TEST_ASSERT_EQUAL(_100BytesBufferSize - 50, circular_space_available(circ));
	TEST_ASSERT_EQUAL(50, circular_bytes_available(circ));


}

TEST(circular, test_make_full)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char byte_data = 'k';

	//
	for(int i = 1; i <= _100BytesBufferSize; i++)
	{
		TEST_ASSERT_EQUAL(1, circular_insert(circ, &byte_data, 1));
		TEST_ASSERT_EQUAL(i, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - i, circular_space_available(circ));
		if(i < _100BytesBufferSize)
		{
			TEST_ASSERT_EQUAL(0, overflow_detection_flag);
		}
		else
		{
			TEST_ASSERT_EQUAL(1, overflow_detection_flag);
			TEST_ASSERT_EQUAL(_100BytesBufferSize, circular_bytes_available(circ));
			TEST_ASSERT_EQUAL(0, circular_space_available(circ));
		}
	}

	//
	TEST_ASSERT_EQUAL(0, circular_insert(circ, &byte_data, 1));
}

TEST(circular, test_receive_block_amount_equals_inserted)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data[50];
	circular_insert(circ, data, 50);

	//
	char buffer[50];

	//
	TEST_ASSERT_EQUAL(50, circular_receive(circ, buffer, 50));
	TEST_ASSERT_EQUAL(0, circular_bytes_available(circ));
	TEST_ASSERT_EQUAL(_100BytesBufferSize, circular_space_available(circ));
}

TEST(circular, test_receive_more_than_available)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data[50];
	circular_insert(circ, data, 50);

	//
	char buffer[60];
	TEST_ASSERT_EQUAL(50, circular_receive(circ, buffer, 60));
	TEST_ASSERT_EQUAL(0, circular_bytes_available(circ));
}

TEST(circular, test_receive_less_than_available)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data[50];
	circular_insert(circ, data, 50);

	//
	char buffer[20];
	TEST_ASSERT_EQUAL(20, circular_receive(circ, buffer, 20));
}


TEST(circular, test_single_byte_stream)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data;
	char buffer;

	//
	for(int i = 0; i < 1000; i++)
	{
		TEST_ASSERT_EQUAL(1, circular_insert(circ, &data, 1));
		TEST_ASSERT_EQUAL(1, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 1, circular_space_available(circ));
		TEST_ASSERT_EQUAL(1, circular_receive(circ, &buffer, 1));
		TEST_ASSERT_EQUAL(0, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize, circular_space_available(circ));
	}
}

TEST(circular, test_block_stream)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data[20];
	char buffer[20];

	//
	for(int i = 0; i < 50; i++)
	{
		//insert block
		TEST_ASSERT_EQUAL(20, circular_insert(circ, data, 20));
		TEST_ASSERT_EQUAL(20, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 20, circular_space_available(circ));
		TEST_ASSERT_EQUAL(20, circular_insert(circ, data, 20));
		TEST_ASSERT_EQUAL(40, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 40, circular_space_available(circ));

		TEST_ASSERT_EQUAL(10, circular_receive(circ, buffer, 10));
		TEST_ASSERT_EQUAL(30, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 30, circular_space_available(circ));
		TEST_ASSERT_EQUAL(10, circular_receive(circ, buffer, 10));
		TEST_ASSERT_EQUAL(20, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 20, circular_space_available(circ));
		TEST_ASSERT_EQUAL(10, circular_receive(circ, buffer, 10));
		TEST_ASSERT_EQUAL(10, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 10, circular_space_available(circ));
		TEST_ASSERT_EQUAL(10, circular_receive(circ, buffer, 10));
		TEST_ASSERT_EQUAL(0, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 0, circular_space_available(circ));
	}
}

TEST(circular, test_block_stream_receive_always_more_than_available)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char data[20];
	char buffer[100];

	//
	for(int i = 0; i < 50; i++)
	{
		//insert block
		TEST_ASSERT_EQUAL(20, circular_insert(circ, data, 20));
		TEST_ASSERT_EQUAL(20, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize - 20, circular_space_available(circ));

		//
		TEST_ASSERT_EQUAL(20, circular_receive(circ, buffer, 100));
		TEST_ASSERT_EQUAL(0, circular_bytes_available(circ));
		TEST_ASSERT_EQUAL(_100BytesBufferSize, circular_space_available(circ));
	}
}

TEST(circular, test_byte_stream_integrity)
{
	//
	circular_t circ = _100BytesCtor();

	char test_char = 'a';
	char test_buffer;

	for(int i = 0; i < 1000; i++)
	{
		circular_insert(circ, &test_char, 1);
		circular_receive(circ, &test_buffer, 1);
		TEST_ASSERT_EQUAL(test_char, test_buffer);
		test_char++;
	}
}

TEST(circular, test_block_stream_integrity)
{
	//
	circular_t circ = _100BytesCtor();

	//
	char block_data[] = "hello this is block data";
	int len = strlen(block_data);

	//
	for(int i = 0; i < 1000; i++)
	{
		len = strlen(block_data);
		circular_insert(circ, block_data, len);
		char buffer[50];
		memset(buffer, 0, 50);
		len = circular_receive(circ, buffer, 100);
		TEST_ASSERT_EQUAL(0, strncmp(block_data, buffer, len));
	}
}





